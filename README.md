# Gulp extension

## Run anonymous functions with metadata

Here is a simple example how you can write gulp tasks without declaring dedicated variables, but also providing metadata such as description

```
const { src, task, dest } = require("gulpty")(require("gulp"));

task("build", done => {
  src(`${DEST_DIR}/**/themes/bui/library.less`)
    .pipe(less())
    .pipe(dest(DEST_DIR));
  done();
}, {description:"build theme"});
```

At the end you will manage to provide the description in CLI:

```
gulp --tasks
[13:16:30] Working directory changed to /gulp-ext
[13:16:34] Tasks for /gulp-ext/example/gulpfile.js
[13:16:34] ├── ui5    extract UI5 resources
[13:16:34] ├── build  build theme
[13:16:34] ├── bui
[13:16:34] └── dist
```
