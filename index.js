module.exports = (gulp) => Object.assign({}, gulp, {
  // helps to run ananymous functions with metadata
  task(displayName, fn, metadata) {

    let task = Object.assign(
      fn,
      {
        displayName
      },
      metadata
    );

    gulp.task(task);
  }
});
